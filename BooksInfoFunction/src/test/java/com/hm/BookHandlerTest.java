package com.hm;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class BookHandlerTest {
  @Test
  public void successfulResponse() {
    BookHandler bookHandler = new BookHandler();
    APIGatewayProxyResponseEvent result = bookHandler.handleRequest(null, null);
    assertEquals(result.getStatusCode().intValue(), 200);
    assertEquals(result.getHeaders().get("Content-Type"), "application/json");
    String content = result.getBody();
    assertNotNull(content);
    assertTrue(content.contains("\"message\""));
    assertTrue(content.contains("\" \""));
    assertTrue(content.contains("\"location\""));
  }
}
