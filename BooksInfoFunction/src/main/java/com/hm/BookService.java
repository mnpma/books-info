package com.hm;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class BookService {
    private final String libUrl = "https://www.googleapis.com/books/v1/volumes";
    private final String libKey = "AIzaSyDIsbsXe7G1OkrFcGsgi_cAGGRiQ1gC_lI";


    public String getBookByISBn(String isbn) throws IOException {
        String url = libUrl + "?q=isbn:0760326576&key=" + libKey;
        HttpGet request = new HttpGet(libUrl);
        //request.addHeader("key", libKey);

/*
*                 .queryParam("q", "isbn:" + isbn)
                .queryParam("key", libraryProperties.getKey()).build();
*/
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            CloseableHttpResponse response = httpClient.execute(request);
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            throw e;
        }
    }
}
